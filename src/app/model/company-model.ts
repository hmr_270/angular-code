export class CompanyModel {

    id: number;
	name: String;
    ceo: String;
    brief: String;
    code: String;
    sector: [];
    boardOfDirectors: [];
    //stockExchangesId: [];
}
