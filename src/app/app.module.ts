import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from  '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { StockMarketComponent } from './components/stock-market/stock-market.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CompanyComponent } from './components/company/company.component';
import { UserComponent } from './components/user/user.component';
import { ComparisonComponent } from './components/comparison/comparison.component';
import { IpoManageComponent } from './components/ipo-manage/ipo-manage.component';
import { AddCompanyComponent } from './components/add-company/add-company.component';
import { UpdateCompanyComponent } from './components/update-company/update-company.component';
import { ViewCompaniesComponent } from './components/view-companies/view-companies.component';
import { ViewSectorsComponent } from './components/view-sectors/view-sectors.component';
import { AddSectorComponent } from './components/add-sector/add-sector.component';
import { SectorComponent } from './components/sector/sector.component';
import { UpdateSectorComponent } from './components/update-sector/update-sector.component';
import { AddCompanySectorComponent } from './components/add-company-sector/add-company-sector.component';
import { DeleteCompanySectorComponent } from './components/delete-company-sector/delete-company-sector.component';
import { AddCompanyBodComponent } from './components/add-company-bod/add-company-bod.component';
import { DeleteCompanyBodComponent } from './components/delete-company-bod/delete-company-bod.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    StockMarketComponent,
    DashboardComponent,
    CompanyComponent,
    UserComponent,
    ComparisonComponent,
    IpoManageComponent,
    AddCompanyComponent,
    UpdateCompanyComponent,
    ViewCompaniesComponent,
    ViewSectorsComponent,
    AddSectorComponent,
    SectorComponent,
    UpdateSectorComponent,
    AddCompanySectorComponent,
    DeleteCompanySectorComponent,
    AddCompanyBodComponent,
    DeleteCompanyBodComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
